import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
	static Product product;
	static List<Product>productList=new ArrayList<>();
	 static ArrayList<String> li=new ArrayList<>();
	 static Product productNametoUpdate;
	 static int productIndex;

	public static void main(String args[]) throws IOException {
		int optionNumber = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("** Welcome to product management system **");
		loadproduct();
		
		while (optionNumber != 5) {
			showOptions();
			optionNumber = 0;
			System.out.println("Enter the option number:\n\n");
			optionNumber = sc.nextInt();
			if (optionNumber == 1) {
				addProduct();

			} else if (optionNumber == 2) {
				updateproduct(); 

			} else if (optionNumber == 3) {
				deleteProduct();

			} else if (optionNumber == 4) {
				

			} else if (optionNumber == 5) {
				writeArryListToFile();
				System.exit(0);
			} 
			else {
				System.out.println("Invalid Option");
			}
		}
	}

	public static void showOptions() {
		System.out.println("\tChoose Operations:");
		System.out.println("\tAdd product");
		System.out.println("\tUpdate product");
		System.out.println("\tSearch product");
		System.out.println("\tDelete product");
		System.out.println("\tExit");

	}

	public static void addProduct() throws IOException {
		System.out.println("***Add Product \n\n");
		Scanner sc = new Scanner(System.in);
		System.out.println("Product Name:");
		String productName = sc.nextLine();
		System.out.println("Product Price \n\n");
		int  price = sc.nextInt();
		System.out.println("Product Quantity:");
		double quantity = sc.nextInt();
		li.add(productName +" "+price+" "+quantity+"\n");
		
	}
	
	public static void updateproduct() {
		System.out.println("***update product**\n\n");
		System.out.println("Enter product you want to update:");
		Scanner sc=new Scanner(System.in);
		String ProductNametoUpdate=sc.nextLine();
		System.out.println("Product Name:");
		String productName = sc.nextLine();
		System.out.println("Product Price \n\n");
		int  price = sc.nextInt();
		System.out.println("Product Quantity:");
		double quantity = sc.nextInt();
		
		li.set(getindexof(ProductNametoUpdate),productName+" "+price+" "+quantity);
		
		
	}
	public static void deleteProduct() {
		System.out.println("*****Delete Product****");
		System.out.println("Enter product you want to delete: ");
		Scanner sc=new Scanner(System.in);
		String productNameTodelete=sc.nextLine();
		
		li.remove(getindexof(productNameTodelete));
	}
	
	

	
	
	public static void loadproduct() throws IOException  {
		String path="C:\\Users\\HP\\eclipse-workspace\\ProductManegment17nov\\src\\Record";
		File file=new File (path);
		FileReader fr=new FileReader(file);
		BufferedReader br=new BufferedReader(fr);
		String line="";
		while((line=br.readLine())!=null) {
			li.add(line);
		}
		
	}
	public static int getindexof(String itemName) {
		int i=0;
		for(String s:li) {
			if(s.contains(itemName)) {
				return i;
			}
			i=i+1;
		}
		return -1;
	}
	public static void writeArryListToFile() throws IOException {
		String filepath;
		
		File file=new File("C:\\Users\\HP\\eclipse-workspace\\ProductManegment17nov\\src\\Record");
		FileWriter fw=new FileWriter(file);
		BufferedWriter bw=new BufferedWriter(fw);
		for(String s:li) {
			bw.write(s+"\n");
			
		}
		bw.close();
		fw.close();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
